#SETUP

## Build and Deploy API

For use this api, it is necessary that you have Workbench and docker installed in the machine.
So you should execute the following commands:

```bash
git clone https://olavobs@bitbucket.org/olavobs/flight-api.git
cd REPOSITORY_PACKAGE/api
mvn clean package
docker build -t NAME_OF_IMAGE .
cd ..
docker-compose up --builc
```

## Use
For use this api you can do the following request:

### For get information of flights from OPORTO to LISBON use: 

```bash
http://localhost:8081/flight-information/OPO-TO-LIS
```

### For get information of flights from LISBON to OPORTO use:

```bash
http://localhost:8081/flight-information/LIS-TO-OPO
```

### It is possible to use also the following parameters to include in the endpoint:

```bash
date_from -> Search tickets information after this date
date_to -> Search tickets information before this date
currency -> Search the currency of the tickets between EUR and GBP (The default currency is EUR)
```
### For get information about the history of requests made use: 

```bash
http://localhost:8081/request
```

### For delete all information about the history of requests made, using the delete method, use:

```bash
http://localhost:8081/request
```

