package com.br.cocus.flightapi.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.br.cocus.flightapi.model.Request;
import com.br.cocus.flightapi.repository.RequestRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase
public class RequestRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private RequestRepository requestRepository;

	@Before
	public void setUp() throws Exception {
		String date1 = "10-09-2020";

		String date2 = "10-10-2020";

		String dest = "LIS-TO-OPO";
		Request r = new Request(dest, Calendar.getInstance().getTime(), date1, date2, "EUR");
		entityManager.persist(r);
		entityManager.flush();
	}

	@Test
	public void findAllTest() {
		int size = requestRepository.findAll().size();
		assertThat(size).isEqualTo(1);
	}

}