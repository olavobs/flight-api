package com.br.cocus.flightapi.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.br.cocus.flightapi.model.Flight;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class AverageServiceTest {

	@Autowired
	private AverageService averageService;

	@Test
	public void calculateFlightsPriceAverageTeste() {
		List<Flight> flights = new ArrayList<Flight>();
		Map<String, Integer> bags_price = new HashMap<String, Integer>();
		bags_price.put("1", 26);
		bags_price.put("2", 84);
		Flight f1 = new Flight("OPO", "LIS", 86, bags_price);
		Flight f2 = new Flight("OPO", "LIS", 88, bags_price);
		flights.add(f1);
		flights.add(f2);

		assertThat(87).isEqualTo(averageService.calculateFlightsPriceAverage(flights));
	}

	@Test
	public void calculateFlightsBagsAverageTest() {
		List<Flight> flights = new ArrayList<Flight>();
		Map<String, Integer> bags_price = new HashMap<String, Integer>();
		bags_price.put("1", 26);
		bags_price.put("2", 84);
		Flight f1 = new Flight("OPO", "LIS", 86, bags_price);
		Map<String, Integer> bags_price1 = new HashMap<String, Integer>();
		bags_price1.put("1", 28);
		bags_price1.put("2", 86);
		Flight f2 = new Flight("OPO", "LIS", 88, bags_price1);
		flights.add(f1);
		flights.add(f2);

		assertThat(27).isEqualTo(averageService.calculateFlightsBagsAverage(flights).get("1"));
		assertThat(85).isEqualTo(averageService.calculateFlightsBagsAverage(flights).get("2"));
	}

}