package com.br.cocus.flightapi.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class JsonHolder {

    @Getter
    @Setter
    private List<Flight> data;

    @Getter
    @Setter
    private Object search_id;
}
