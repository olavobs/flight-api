package com.br.cocus.flightapi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "request")
@NoArgsConstructor
public class Request {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@Getter
	private Long requestId;

	@CreatedDate
	@Column(name = "created_date")
	private Date createdDate;

	@Getter
	@Setter
	private String flight;

	@Column
	@Getter
	@Setter
	private String date_from;

	@Column
	@Getter
	@Setter
	private String date_to;

	@Column
	@Getter
	@Setter
	private String curr;

	public Request(String flight, Date createdDate, String dateFrom, String dateTo, String curr) {
		this.flight = flight;
		this.createdDate = createdDate;
		this.date_from = dateFrom;
		this.date_to = dateTo;
		this.curr = curr;
	}

}