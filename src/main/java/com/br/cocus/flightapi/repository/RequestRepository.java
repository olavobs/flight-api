package com.br.cocus.flightapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.cocus.flightapi.model.Request;

public interface RequestRepository extends JpaRepository<Request, Long> {

}