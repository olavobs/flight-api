package com.br.cocus.flightapi.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.br.cocus.flightapi.model.Flight;

@Service
public class AverageService {

	public int calculateFlightsPriceAverage(List<Flight> flights) {
		int flightsCount = flights.size();
		int flightPriceSum = 0;
		for (Flight f : flights) {
			flightPriceSum += f.getPrice();
		}
		return (flightPriceSum / flightsCount);
	}

	public Map<String, Integer> calculateFlightsBagsAverage(List<Flight> flights) {
		Map<String, Integer> bags = new HashMap<String, Integer>();
		int flightsCount = flights.size();
		int flightBag1Sum = 0;
		int flightBag2Sum = 0;
		for (Flight f : flights) {
			if (f.getBags_price().get("1") != null) {
				flightBag1Sum += f.getBags_price().get("1");
			}
			if (f.getBags_price().get("2") != null) {
				flightBag2Sum += f.getBags_price().get("2");
			}
		}
		bags.put("bag_1_avg", flightBag1Sum / flightsCount);
		bags.put("bag_2_avg", flightBag2Sum / flightsCount);
		return bags;
	}

}