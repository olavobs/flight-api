package com.br.cocus.flightapi.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.br.cocus.flightapi.model.JsonHolder;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FlightService {

	private static final String BASE_API_URL = "https://api.skypicker.com/flights";

	public JsonHolder callKiwiAPI(String flyFrom, String flyTo, String url) {
		String urlRequest = "";
		urlRequest += BASE_API_URL + "?fly_from=" + flyFrom + "&fly_to=" + flyTo + "&partner=picky&curr=EUR" + url;
		log.info(urlRequest);

		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForEntity(urlRequest, JsonHolder.class).getBody();

	}
}