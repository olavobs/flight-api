package com.br.cocus.flightapi.controller;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.br.cocus.flightapi.dto.FlightAverageDTO;
import com.br.cocus.flightapi.model.JsonHolder;
import com.br.cocus.flightapi.model.Request;
import com.br.cocus.flightapi.repository.RequestRepository;
import com.br.cocus.flightapi.service.AverageService;
import com.br.cocus.flightapi.service.FlightService;

@RestController
@RequestMapping(path = "/flight-information")
public class FlightController {

	private AverageService averageService;
	private FlightService flightService;
	private RequestRepository requestRepository;

	@Autowired
	public FlightController(AverageService averageService, FlightService flightService,
			RequestRepository requestRepository) {
		this.averageService = averageService;
		this.flightService = flightService;
		this.requestRepository = requestRepository;
	}

	@RequestMapping(value = "/OPO-TO-LIS", produces = "application/json", method = RequestMethod.GET)
	public ResponseEntity<?> getFlightInformationOpoToLis(
			@RequestParam(value = "date_from", defaultValue = "") String date_from,
			@RequestParam(value = "date_to", defaultValue = "") String date_to,
			@RequestParam(value = "currency", defaultValue = "EUR") String currency) {

		Request request = new Request("OPO-TO-LIS", Calendar.getInstance().getTime(), date_from, date_to, currency);
		Map<String, FlightAverageDTO> dtos = new HashMap<String, FlightAverageDTO>();
		requestRepository.save(request);

		String url = String.format("&date_from=%s&date_to=%s&currency=%s", date_from, date_to, currency);

		JsonHolder response = flightService.callKiwiAPI("OPO", "LIS", url);
		if (response != null) {
			int priceAvg = averageService.calculateFlightsPriceAverage(response.getData());
			Map<String, Integer> bags = averageService.calculateFlightsBagsAverage(response.getData());

			FlightAverageDTO dto = new FlightAverageDTO(currency, priceAvg, bags);
			dtos.put("OPO-TO-LIS", dto);
		}
		if (!dtos.isEmpty()) {
			return ResponseEntity.ok().body(dtos);
		}
		return ResponseEntity.badRequest().build();
	}

	@RequestMapping(value = "/LIS-TO-OPO", produces = "application/json", method = RequestMethod.GET)
	public ResponseEntity<?> getFlightInformationLisToOpo(
			@RequestParam(value = "date_from", defaultValue = "") String date_from,
			@RequestParam(value = "date_to", defaultValue = "") String date_to,
			@RequestParam(value = "currency", defaultValue = "EUR") String currency) {

		Request request = new Request("LIS-TO-OPO", Calendar.getInstance().getTime(), date_from, date_to, currency);
		Map<String, FlightAverageDTO> dtos = new HashMap<String, FlightAverageDTO>();
		requestRepository.save(request);

		String url = String.format("&date_from=%s&date_to=%s&currency=%s", date_from, date_to, currency);

		JsonHolder response = flightService.callKiwiAPI("LIS", "OPO", url);
		if (response != null) {
			int priceAvg = averageService.calculateFlightsPriceAverage(response.getData());
			Map<String, Integer> bags = averageService.calculateFlightsBagsAverage(response.getData());

			FlightAverageDTO dto = new FlightAverageDTO(currency, priceAvg, bags);
			dtos.put("LIS-TO-OPO", dto);
		}
		if (!dtos.isEmpty()) {
			return ResponseEntity.ok().body(dtos);
		}
		return ResponseEntity.badRequest().build();
	}
}